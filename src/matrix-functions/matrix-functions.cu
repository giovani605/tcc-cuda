#include <cuda.h>

//#include "../matrix-utils/matrix-utils.h"

//__global__  void multiplicacaoMatriz(Matriz* matrizEntrada, Matriz* matrizVetor, Matriz* matrizResultado) {
//	// posicionar
//	// a matriz eh flat
//
////	float memCompartilhada = 0;
////	int posmatriz = (blockIdx.x * colunasMatriz) + threadIdx.x;
////	// a vetor eh flat e soh vou precisa de 1 elemento
////	int posvetor = threadIdx.x;
////	int posResultado = blockIdx.x;
////
////	int indexMaxMatriz = (linhasMatriz * colunasMatriz);
////	int indexMaxVetor = (linhasVetor * colunasVetor);
////	int indexMaxResultado = (linhasResultado * colunasResultado);
////
////	if (posmatriz < indexMaxMatriz && posvetor < indexMaxVetor) {
////		//float a = matrizEntrada[posmatriz];
////		//float b = vetorEntrada[posvetor];
////		// usar memoria do block
////		memCompartilhada += 3;
////
////	}
////	if (posResultado < indexMaxResultado) {
////		matrizResultado[posResultado] += memCompartilhada;
////	}
//
//}

__global__ void multiplicacaoMatriz(float* matrizEntrada, float* vetorEntrada,
		float* matrizResultado, int linhasMatriz, int colunasMatriz,
		int linhasVetor, int colunasVetor, int linhasResultado,
		int colunasResultado) {


	// for para omar os elementos
	// sem usar threads
	float soma = 0;
	int startMatriz = (blockIdx.x * colunasMatriz);
	int endMatriz = startMatriz + colunasMatriz;

	int indexMaxMatriz = (linhasMatriz * colunasMatriz);
	int indexMaxVetor = (linhasVetor * colunasVetor);

	if(startMatriz < indexMaxMatriz && endMatriz <= indexMaxMatriz){
		for(int incrementador = startMatriz; incrementador < endMatriz; incrementador++){
			soma += (matrizEntrada[incrementador] * vetorEntrada[incrementador - startMatriz]);
		}
	}


	int posResultado = blockIdx.x * colunasResultado + blockIdx.y;
	int indexMaxResultado = (linhasResultado * colunasResultado);
	if (posResultado < indexMaxResultado) {
		matrizResultado[posResultado] += soma;
	}

}
