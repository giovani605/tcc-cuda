#include <cuda.h>


//__global__ void multiplicacaoMatriz(Matriz* matrizEntrada, Matriz* matrizVetor,
//		Matriz* matrizResultado)

__global__ void multiplicacaoMatriz(float* matrizEntrada, float* vetorEntrada,
		float* matrizResultado, int linhasMatriz, int colunasMatriz,
		int linhasVetor, int colunasVetor, int linhasResultado,
		int colunasResultado);
