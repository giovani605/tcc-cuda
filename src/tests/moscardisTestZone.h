void zonaDeTestes();

__global__ void kernalDeMultiplicacao(unsigned int numeroLinhasMatriz,
		unsigned int numeroColunasMatriz, float* matriz,
		unsigned int numeroLinhasVetor, unsigned int numeroColunasVetor,
		float* vetor, float* resultante);
