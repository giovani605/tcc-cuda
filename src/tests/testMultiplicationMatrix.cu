#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include <cuda.h>

#include "testMultiplicationMatrix.h"

#include "../matrix-utils/matrix-utils.h"

#include "../matrix-functions/matrix-functions.h"

void multiplicarMatrixVetor() {

	std::cout << "CUDA Program" << std::endl;

	Matriz* matrizDeExibicao = gerarStructMatriz(10, 10);
	preencherMatrizComNumeracaoCreescente(matrizDeExibicao);
	printMatriz(matrizDeExibicao);

	// gerar matriz
	Matriz* matrizDeMultiplicacao = gerarStructMatriz(10, 10);
	preencherMatrizComNumeracaoCreescente(matrizDeMultiplicacao);
	// gerar vetor
	Matriz* vetorDeMultiplicacao = gerarStructMatriz(10, 1);
	preencherMatrizComNumeracaoCreescente(vetorDeMultiplicacao);
	printMatriz(vetorDeMultiplicacao);
	// verificar se eles a multiplicacao eh possivel
	if (!verificarMultMatrizVetor(matrizDeMultiplicacao,
			vetorDeMultiplicacao)) {
		std::cout << "Matriz e vetor com tamanhos errados";
		return;
	}

	//gerar espaco para a matriz resultante
	Matriz* matrizResultante = gerarStructMatriz(
			matrizDeMultiplicacao->numeroLinhas,
			vetorDeMultiplicacao->numeroColunas);
	preencherMatrizComNumeracaoCreescente(matrizResultante);
	std::cout << "Matriz e vetor com tamanhos corretos";
	quebraLinha();

	simularMultiplicacaoNoHost(matrizDeMultiplicacao, vetorDeMultiplicacao);

//
//	std::cout << "Print matriz entrada" << std::endl;
//	printMatrizFlat(matrizDeMultiplicacao->matriz, matrizDeMultiplicacao->linhas, matrizDeMultiplicacao->colunas);
//	std::cout << "Print vetor entrada" << std::endl;
//	printMatrizFlat(v->vetor, v->linhas, v->colunas);
//
//	// alocar memoria no cuda
	float* matrizCuda;
	float* vetorCuda;
	float* matrizResultadoCuda;
//
	cudaMalloc(&matrizCuda, matrizDeMultiplicacao->tamanhoFloatArray);
	cudaMalloc(&vetorCuda, vetorDeMultiplicacao->tamanhoFloatArray);
	cudaMalloc(&matrizResultadoCuda, matrizResultante->tamanhoFloatArray);

	// copiar valores
	cudaMemcpy(matrizCuda, matrizDeMultiplicacao->matriz,
			matrizDeMultiplicacao->tamanhoFloatArray, cudaMemcpyHostToDevice);
	cudaMemcpy(vetorCuda, vetorDeMultiplicacao->matriz,
			vetorDeMultiplicacao->tamanhoFloatArray, cudaMemcpyHostToDevice);
	// configurar valores do kernel e exucutar
	int threadsPerBlock = 1;
	dim3 blocksPerGrid(matrizResultante->numeroLinhas,
			vetorDeMultiplicacao->numeroColunas);
	multiplicacaoMatriz<<<blocksPerGrid, threadsPerBlock>>>(matrizCuda,
			vetorCuda, matrizResultadoCuda, matrizDeMultiplicacao->numeroLinhas,
			matrizDeMultiplicacao->numeroColunas,
			vetorDeMultiplicacao->numeroLinhas,
			vetorDeMultiplicacao->numeroColunas, matrizResultante->numeroLinhas,
			matrizResultante->numeroColunas);

	cudaDeviceSynchronize();

	// trazer os valores de volta
	cudaMemcpy(matrizResultante->matriz, matrizResultadoCuda,
			matrizResultante->tamanhoFloatArray, cudaMemcpyDeviceToHost);
	std::cout << "matriz resultado" << std::endl;
	printMatriz(matrizResultante);
}
